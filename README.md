Написанные роли Ansible:
- deploy_server: разворачивает приложение [script-server](https://github.com/bugy/script-server) и nginx в контейнерах при помощие docker-compose. Доступ к script-server осуществляется только через nginx, который настроен как обратный прокси сервер.
- hostname - редактирует hostname целевого сервера, имя берется из файла реестра (inventory_hostname).
- editor_time - синхронизирует время целевого сервера по протоколу NTP с сервером NTP, который задается, как переменная роли.
- installer_docker - устанавливает docker и docker-compose на целевой сервер.
- installer_libs - устанавливает библиотеки из переменной роли pckg на целевой сервер.
